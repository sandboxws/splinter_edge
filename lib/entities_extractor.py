#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import nltk
import urllib2
import re
from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer

stopwords = stopwords.words('english')

# Used when tokenizing words
sentence_re = r'''(?x)           # set flag to allow verbose regexps
      ([A-Z])(\.[A-Z])+\.?       # abbreviations, e.g. U.S.A.
    | \w+\.js                    # JavaScript frameworks, e.g. ember.js
    | \w+(-\w+)*                 # words with optional internal hyphens
    | \$?\d+(\.\d+)?%?           # currency and percentages, e.g. $12.40, 82%
    | \.\.\.                     # ellipsis
    | [][.,;"'?():-_`]           # these are separate tokens
'''

lemmatizer = nltk.WordNetLemmatizer()
stemmer = nltk.stem.porter.PorterStemmer()

#Taken from Su Nam Kim Paper...
grammar = r"""
    NBAR:
        {<NN.*|JJ>*<NN.*>}  # Nouns and Adjectives, terminated with Nouns

    NP:
        {<NBAR><IN><NBAR>}  # Above, connected with in/of/etc...
        {<NBAR>}
"""
chunker = nltk.RegexpParser(grammar)

class EntitiesExtractor:
    def __init__(self, text):
        self.html = text
        text = nltk.clean_html(text)
        text = self.normalise(text)
        self.text = self.removeNonAscii(text)
        toks = nltk.regexp_tokenize(self.text, sentence_re)
        postoks = nltk.tag.pos_tag(toks)
        self.tree = chunker.parse(postoks)
        self.tokenizer = RegexpTokenizer(r'\w+')
    def removeNonAscii(self,s): return "".join(filter(lambda x: ord(x)<128, s))
    def leaves(self,tree):
        """Finds NP (nounphrase) leaf nodes of a chunk tree."""
        for subtree in tree.subtrees(filter = lambda t: t.node=='NP'):
            yield subtree.leaves()

    def normalise(self,word):
        """Normalises words to lowercase and stems and lemmatizes it."""
        word = word.lower()
        word = re.sub('[-_]', ' ', word)
        try:
            word = unicode(word, 'utf-8', 'ignore')
        except TypeError:
            return word
        #word = self.tokenizer.tokenize(word)[0]
        #word = stemmer.stem_word(word)
        #word = lemmatizer.lemmatize(word)
        return word

    def acceptable_word(self,word):
        """Checks conditions for acceptable word: length, stopword."""
        accepted = bool(2 <= len(word) <= 40)
        return accepted

    def get_terms(self,tree):
        leaves = self.leaves(self.tree)
        for leaf in leaves:
            term = [ w for w,t in leaf if self.acceptable_word(w) ]
            yield term

    def get_word_weight(self,word):
        """Returns the weight for a word in the html text"""
        return self.text.count(word)

    def get_entities(self):
        """Returns extracted entities with associate weight/score"""

        terms = self.get_terms(self.tree)
        entities = dict()
        for words in terms:
            entity = ' '.join(words)
            entity = entity.strip()
            size = len(entity.split(' '))
            if size <= 3 and len(entity) >= 2 and entity not in entities:
                weight = self.get_word_weight(entity)
                entities[entity] = { 'score': weight }
                print entity
        entities = sorted(entities.items(), key=lambda x: (x[1],x[0]), reverse=True)
        return entities

text = """

"""
EntitiesExtractor(text).get_entities()
