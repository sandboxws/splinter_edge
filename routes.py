import os
import urllib2
from flask import Flask
from lib.entities_extractor import EntitiesExtractor
from flaskext.jsonify import jsonify
from flask import request
app = Flask(__name__)

@app.route('/get_url_entities', methods=['POST'])
@jsonify
def get_entities_from_url():
    url = request.form['url']
    response = urllib2.urlopen(url)
    html = response.read()
    e = EntitiesExtractor(html)
    entities = e.get_entities()

    return entities

@app.route('/get_text_entities', methods=['POST'])
@jsonify
def get_entities_from_text():
    text = request.form['text']
    e = EntitiesExtractor(text)
    entities = e.get_entities()

    return entities

if __name__ == "__main__":
    env = os.getenv('RAILS_ENV', 'development')
    if env == 'development':
        app.run(debug=True)
    else:
        app.run(host='198.199.67.133')
